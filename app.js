/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, gamePlaying, prevDice, winningScore;

init();


/////////// ROLL BUTTON

document.querySelector('.btn-roll').addEventListener('click', function() {
    if(gamePlaying) {
        
         
         // 1. Random number
         var dice = Math.floor(Math.random() * 6) + 1;
         var dice2 = Math.floor(Math.random() * 6) + 1;
    
         // 2. Display the result
         var diceDOM = document.querySelector('.dice');
         var dice2DOM = document.querySelector('.dice2');
             diceDOM.style.display = 'block';
             dice2DOM.style.display = 'block';
         diceDOM.src = 'dice-' + dice + '.png';
         dice2DOM.src = 'dice-' + dice2 + '.png';
         
            
               
        //Check if latest 2 dices are 6
        
//        if(prevDice === dice && dice == 6) {
//            roundScore = 0;
//            scores[activePlayer] = 0;
//            nextPlayer();
//            dice = 0;
//        }
        
        //Check if both dices are 6
            
            if(dice === 6 && dice2 === 6) {
                roundScore = 0;
                scores[activePlayer] = 0;
                nextPlayer();
                dice = 0;
                dice2 = 0;
        document.getElementById('roll-0').style.visibility = 'hidden';     
        document.getElementById('roll-1').style.visibility = 'hidden'; 
        document.getElementById('six-10').style.visibility = 'hidden'; 
        document.getElementById('six-11').style.visibility = 'hidden'; 
        document.getElementById('six-1' + activePlayer).style.visibility = 'visible';
            }
        
         // 3. Update the round score IF the rolled number was NOT a 1
         else if(dice !== 1 && dice2 !== 1) {
             
             //Add score
        roundScore = roundScore + dice + dice2;
        document.querySelector('#current-' + activePlayer).textContent = roundScore;
        document.getElementById('roll-0').style.visibility = 'hidden';     
        document.getElementById('roll-1').style.visibility = 'hidden'; 
        document.getElementById('six-10').style.visibility = 'hidden'; 
        document.getElementById('six-11').style.visibility = 'hidden'; 
              
         } else {
             //Next player
             document.getElementById('roll-0').style.visibility = 'hidden';     
             document.getElementById('roll-1').style.visibility = 'hidden'; 
             document.getElementById('six-10').style.visibility = 'hidden'; 
             document.getElementById('six-11').style.visibility = 'hidden'; 
             document.getElementById('roll-' + activePlayer).style.visibility = 'visible';
             nextPlayer();
             };
        // Save previous dice
        prevDice = dice;
    }
    
});

/////////// HOLD BUTTON

document.querySelector('.btn-hold').addEventListener('click', function() {
    if(gamePlaying) {
        // Add CURRENT score to GLOBAL score
        scores[activePlayer] += roundScore;

        // Update the UI
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

        // Check if player won the game
        if(scores[activePlayer] >= document.getElementById('win-sc').value) {
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
            document.querySelector('.dice').style.display = 'none';
            document.querySelector('.dice2').style.display = 'none';
            document.querySelector('#name-' + activePlayer).textContent = 'winner';
            scores = [0,0];
            gamePlaying = false;
        } else {
             //Next player
            nextPlayer();
        }
    }
});


/////////// NEW GAME BUTTON

document.querySelector('.btn-new').addEventListener('click', init);



///////////// FUNCTIONS used aboved  -- defined below

function nextPlayer() {
     //Next player
        activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
        roundScore = 0;
    
    
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    
    document.querySelector('.player-0-panel').classList.toggle('active');    
    document.querySelector('.player-1-panel').classList.toggle('active');    
        
    //document.querySelector('.player-0-panel').classList.remove('active');
    //document.querySelector('.player-1-panel').classList.add('active');
        
    document.querySelector('.dice').style.display = 'none';
    document.querySelector('.dice2').style.display = 'none';
};



function init() {    
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    
    gamePlaying = true;
    
    document.querySelector('.dice').style.display = 'none';
    document.querySelector('.dice2').style.display = 'none';

    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.getElementById('win-sc').value = '25';
    
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.add('active');

       
}

function popUp() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}

























/////////////////////////////////////////////////////////
//dice = Math.floor(Math.random() * 6) + 1;

//document.querySelector('#current-' + activePlayer).textContent = dice;
//document.querySelector('#current-' + activePlayer).innerHTML = '<em>' + dice + '<em>';

//var x = document.querySelector('#score-0').textContent;
//console.log(x);





























































